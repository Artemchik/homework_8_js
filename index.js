//                                   1
document.querySelectorAll("p").forEach(elem => elem.style.backgroundColor = "#ff0000")
//                                   2
document.querySelectorAll("#optionsList").forEach(elem =>{
    console.log("Node name:",elem.nodeName);
    console.log("Node type:",elem.nodeType);
})
console.log(document.querySelector("#optionsList").parentElement);
document.querySelector("#optionsList").childNodes.forEach(elem => console.log(elem));
//                                   3
document.querySelector("#testParagraph").textContent = " This is a paragraph ";
//                                   4,5
const headerChildren = document.querySelector(".main-header").children;
for( let collection  of headerChildren) {
    collection.classList.add("nav-item");
}
//                                   6
 document.querySelectorAll(".section-title").forEach(elem => elem.classList.remove("section-title"));

